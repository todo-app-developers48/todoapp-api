![Status](https://gitlab.com/todo-app-developers48/todoapp-api/badges/master/pipeline.svg)

[[_TOC_]]

## Project :tada:

The project is developed using Node.js. To get ready, install packages using NMP:

```shell
npm install
```
### JavaScript calls

A simple JavaScript call to launch the service can be written as:
``` JavaScript
function launch() {
    // handle the server launch.
}

app.start(1234, launch);
```
### Node.js requirements

A Node.js app requires three things:
- [ ] NPM for package managemnet
- [ ] JavaScript for programming interface
- [x] Passion, and sometimes patience. :wink:

### Project structure

```mermaid
graph TD
   A[JavaScript] --> |Compiles down to| B(V8)
```


### Node Expreess template project

This project is is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.
